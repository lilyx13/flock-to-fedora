# Moodboard | README
_Links and General Notes_

## Links
- [Ashlyn's Moodboard Penpot file](https://design.penpot.app/#/view/bc8965f0-3054-11ec-83dc-f35c6ae458e9?page-id=bc8965f1-3054-11ec-83dc-f35c6ae458e9&index=0&share-id=f5542a90-3055-11ec-83dc-f35c6ae458e9)
  - A final version of this will be exported and saved in this repo

## Notes
- These are layouts and designs that I thing will be useful for the design of this website.