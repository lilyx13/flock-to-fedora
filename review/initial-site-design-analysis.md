# Layout and Design Analysis
_Techniques used to visually organize content_

## Critical Overview
- Broken layout (horizontal scroll)
- Very long
- Lots of text
- Does not make use of mulitple columns or flexible layout
- Inconsistent use of white space
- Unresponsive Fonts
- Does use correct typography
- No buttons for user interaction
- No images apart from hero
- Does not apply the rule of 3
- some content/headings are off center
- Nest/Flock branding are inconsistent

#### Mobile Specific
- Horizontal Scroll causes hamburger menu to be off screen
- no mobile optimized navigation methods
- Sticky topbar requires a long upswipe to reactivate
- menu does not open a mobile layout
- Menu does not have quick signup or contact links or anything.
  - user is required to do a lot of scrolling and remembering to search this site effectively
- Informational text blocks have too many lines and are very narrow, makes it hard to focus on reading
- Text blocks are not all centered

## Styles
_branding, design concepts_
- Seems to follow fedora brand ok, but is old branding
- Desktop first design (no mobile optimization)
- Sponsors has 1 icon that is stylistically inconsistent
- Sponsors titles are buried and the content is not clearly structured in relation to them.
- Very little visuals, considering this is advertising an annual get together, it feels very... unpopulated to me
- footer design should be consistent across fedora web ecosystem.

### Critique and revamp guiding ideas
- Style should embody the 4 core values
  - First: use modern and innovative design
  - Friends: show people and communicate the community aspect of Fedora visually
  - Features: page should have features that make it really easy to navigate
    - buttons, easy navigation...
  - Freedom: for everyone to be able to use, this page should support
    - languages
    - accessibility needs
    - load fast and light (optimized assets)