# All Things Open Site Analysis

- Website: [All things Open](https://2021.allthingsopen.org/)

## General Notes
- Best styled page in collection
- Strong color theme
- Every panel is accompanied with at least 1 button for users to engage with
- Page is long
- Some things feel like they should be clickable but arent
---

## Content Analysis
- short sentences
- mostly headings
- in some lists, the font weight is really thick and blurs into background
- Semantically, little superfulous information for the user
  - does not over explain
- no passive voice
- icons used along with headings
  - in-person/virtual
- consistent use of button style for user interaction (all user interaction is done with the same looking items)

## layout and General Design
- Very good mobile-desktop experience
  - content is easy to read and look at on both screens, content such as paralax backgrounds simple on mobile
- header does not stick
- visual hierarchy is fairly consistent
  - ex: sponsors logo sizes vary depending on the type of sponsorship. This pairs well with underline and bold title
- complex background-foreground 3-4 layers
- no fast to the top button in footer

## Readability/Scannability
- Fantasticly done on both desktop and mobile
- no paragraphs longer than 2 sentences.
  - sentence length is short
  - sentences are in active voice
- use of typography variants to aid scanning
- button style is consistent and stands out effectively on desktop and mobile
  - hover color change on desktop
  - good size for mobile
- text in some lists is too heavy
---

## Topic Review

### Above the fold
- mobile version is simple and action oriented but not plain
  - button cloud
    - This effectively allows most users to go to where they need to go without scrolling or opening a menu
    - buttons:
     - event overview
     - register
     - latest news
     - event page
  - Typography
    - caps text, outline text for key words
- desktop version hero is animated and cartoonish
- useful content next to menu
- social media all in the header

### Social Media
- icons in header and footer
  - great for first glance
  - not persistent throughout scanning experience

### Sponsor Engagement
- Become a sponsor button after all sponsors shown
- lots of padding between this button and next visible content

### Call to Participation
- Mailing list close to top and then again at bottom
  - first time is just a button, second time displayed with more information
- no become a spearker button, this was likely removed after there was not a call for participation
- 3 buttons accompany event location information
  - scholarship and free pass
  - description of hybrid
  - why attend
  - **links do not open in new tab**
### Registration
- Register above fold and then after sponsor links (accompanied by a big image that brings together previous images)