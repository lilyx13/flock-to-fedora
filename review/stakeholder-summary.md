# Stakeholder Interview Summary

These notes only include conclusions and action items to use when prototyping the Flock Website.

- Dev Idea: For different stages, have different branches and switch production branch with updated info at important stages

## Site Look and Feel

- This page should feel community oriented and less like a corporate page
- Consistent with Fedora branding and design used in recent start.fpo mockup
- Make use of Fedora design assets
- Some animations on desktop
- Clearly defined interactive elements
  - color and shape
  - use brand colors (magenta for calls to action)

### Descriptive Terms

- Friendly
- Energizing
- Inspiring
- Adventure
- Community

### Notes on Branding

- Flock is Primary Branding
  - Nest is a subset of flock
  - In future Nest will likely be the virtual component of flock

## Target Audiences

- Fedora Contributors
  - Already contributing
- Potential Contributors
  - End users who are interested in getting involved
  - Students and Developers who are interested
- Developers and Content Creators

  - People who would use fedora to create things
    - Web application focused
    - Open source design
    - Containerized workloads
    - Data Sciences
    - Web Developers

### Notes

- End users are less of a focus for this event. While nest has made it more accessible and attracted end users (and this is good), care needs to be taken to how this is marketed.
- This dynamic is along the lines of the shift that Fedora is experiencing
  - Expanding beyond just fedora for fedorans direction
  - Promoting user story of an OS for people to create amazing things with that aren't for fedora

---

## Event Specific Information

Flock was described as being integral to planning future work on Fedora. Especially with ability for people to have impromptu conversations etc.

### Activities

- Lightening Talks
- Hallway Tracks
- Socials
- Hackfests
- Presentations
- Keynote Speakers
- Workshops
- Presentations

### Topics

- This will vary depending on the year and submissions
- However some general topics might be able to be shown all year round
- This will require a follow up with Marie and review of past events

### Event Real Actions

- Flock focused more on teams planning
- Nest focused more on presentation and learning

---

## Takeaways From Page Reviews

- Note that value based critiques of other pages are written in context of what is being looked for for the flock site.

### Current Nest

- Flock Logo Update
- Social Media needs to be handled more strategically
  - Useful and not just there for posterity
  - Hierarchy between platforms
- Design assets should be people centric
- Streamline text content
- Appeal directly to determined user bases
- Design should handle:
  - 'Oh Crap' Situations
  - New/Potential Contributors
  - Year round time sensitive information
- Clean design with light css animations to add interest
- Clear visual hierarchy
- Page should not be too long
- Navigation anchor links down page are good
  - Sticky nav for persitant usefulness
  - Visual distinction for links that will open to a new page or open in a new tab

### [Devconf](https://www.devconf.info/us/)

- Shoutouts to different tech fields
  - does not include UX, was seen as alienating
- Proposal submission in hero ++
- Coherent branding ++
- All participants really liked the conference bullet point section
- Page is too short
- Presence of Code of Conduct ++
  - new tab opening was weird

### [All Things Open](http://allthingsopen.net/)

- Tag line is very well done
- Animations well liked
- Page is too long
- Too many things feel like buttons but aren't
- nav menu shouldn't be same for both desktop and mobile
- Topics section takes up too much space without giving much info
- Redundent content
- Grid layout for sponsors is good

### [Open Source Summit](https://events.linuxfoundation.org/open-source-summit-north-america/)

- Too Corporate
  - Blockiness, color, text
- Focused on high profile presenters
  - Our focus shouldn't place as much value on this as this is a community focused event.
- Good hero section
  - Presence of actual people in hero was liked

### [Guadec](https://events.gnome.org/event/9/)

- Internationalization support is great
  - language and time zone
- Hero is confusing contentwise
- Key features feel almost hidden
