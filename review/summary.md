# Competitor Analysis Summary

## Key Takeaways

Things I learned from these other sites

### Top Level UI and Design

- A top bar search is good to have for quick navigation
  - on desktop seeing anchor links
  - mobile nav should be functional and quick
  - Links to other pages are fine, local stuff should stay in the same tab
- A good hero is extremely important
  - social media
  - relevant image
    - conveys identity and general vibe of the event
  - 1-2 Buttons (Possible ideas)
    - Registeration when it's open
    - Link to Calendar
    - Call for Proposal's when open
    - Go to the Event while it's running
  - 1 Sentence Header
    - should be visually appealing and easy to scan
- Useful for platform/location
  - In person
  - Virtual
  - Hybrid
- Calendar/Schedule Section
  - Something to indicate **what's going on**
    - Links to more information
  - Can be topically sorted or date sorted or a mix. Depends on the information for best way to display
    - If topical, using just headings tends to be not enough information while also taking up a lot of space
- Sponsorship section
  - Ability for new sponsors to contact
  - display in a grid
  - Use size to differentiate sponsorship types, but make sure all are still visible
- Summary Section for Post Event
  - Useful to make the page feel up to date

### Elements

- All interactive content should look and feel similar
- Explanatory text is important but short sentences a must
- Text heavy sections need visual elements to break up text into smaller bites
- Images help a lot. Especially if the event is community focused, otherwise it feels very inaccessible
