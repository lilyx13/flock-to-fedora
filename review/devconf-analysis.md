# Devconf Competitor Analysis
- [Devconf](https://www.devconf.info/cz/)

## General Impressions
- Second most compelling event page
- Some things feel like buttons but aren't
- Better top nav desktop-mobile experience
- Good page length
- Sticky topbar is very useful

---

## Content Analysis
- Lots of long form text
- Complex sentence structure
- Jargon heavy


## Layout and General Design
- Hero side nav for social media on desktop is a cool idea
  - ok on mobile but breaks convention with limited advantage (except for buttons being easy to hit with finger)
- timeline used for topics makes good use of horizontal space on desktop
- About section 2 column on desktop makes the paragraph length feel long but not overbearing
- D is well designed.
  - interesting choice having a mac in the image for a red hat event
- Dates look like they should be clickable but arent  
  - because of use of purple in links, this is bad visual hierarchy
- some clear and some unclear user interaction opportunities down page
- all content can be reached from hero through menu
- to top button in footer is good

## Readability/Scannability
- font weight variation is good
- long form text is too long with few breadcrumbs, but where they do exist is helpful and relevant
- Sentences are good english but include lots of big words and jaron. Not beginner friendly
- timeline tracking still helps scannability on mobile by providing a bit of structure and guiding lines

---

## Topic Review

### Above the fold
- Hero narrative is fragmented
  - Initial content is advertising an event (feeling like for a future event)
    - But call to action is for past content with no direction for user on the change in meaning  
- Virtual event circle looks like a button or like it should do something
  - Is the only time where this color is used

### Social Media
- Interesting use of side bar in hero
  - Style wise this emulates the "D" in the hero image well
  - they also open to a new tab which is good
- No footer social media icons

### Sponsor Engagement
- In footer
- Only redhat based projects
- layout of footer in general does not seem to be well planned out
- No way to be a sponsor on homepage

### Call to Participation
- How to participate link in sticky topbar
  - this is just an anchor the available resources are confusing
    - "What we know!" feels like it's from a support role voice as opposed to organizer voice
- Signup stuff was likely available prior to event.
- Offering content to follow up with is ok but needs better framing

### Calendar
- non interactive

### Registration
- N/A