# Open Sumit Analysis
- [Open Source Summit](https://events.linuxfoundation.org/open-source-summit-north-america/)

## General Impressions
- good indication of timelines on hero
- makes nice use of grid layouts
- not enough white space
- grids render well on mobile
- professional focus
- not jargon heavy

---

## Content Analysis
- no passive voice and limited acronyms/jargon
- pretty good follow up for next year event
- wrap up statement is main long form text
- dropdown bios for speakers
  - some of these are really long and hard to read, especially on mobile
  - having more than just a name is good and the use of dropdown keeps user on the page
- messaging is limited
  - basic idea of event is: A conference about linux where you will hear from big names in the industry
    - apart from this, identity building is kinda limited

## Layout and General Design
- Good use of grid layouts for galleries
  - except for the bottom row of the speakers, a grid layout would have been better than flex for these gallery items
    - except this does look better on mobile than on desktop
- grids transition from desktop to mobile well
- buttons are only at the top of the page, user is passive for rest of the content
- navigation is fairly well done
- mobile site handles dropdown content ok
- mobile menu items are too small
- sticky nav makes up for lack of buttons a little bit

## Readability/Scannability
- text is ok to scan, really small and dense on mobile
- bios are hard to read
- button text is kinda bad contrast

---

## Topic Review

### Above the fold
- best part of the site
- communicates identity of the event fairly well through use of hero image
- on mobile CTA buttons are below the fold

### Social media
- in footer
- afterthought

### Call to Participation
- As this is post event, this is limited
- However it looks like the user is engaged with this at the top and nowhere else

### Sponsor Engagement
- displayed on home page
- if you want to sponsor the home page does not give action item for that
- you must access this through nav bar

### Call to Participation
- in the hero and topbar
- newsletter signup is in the footer
- newsletter text is not eye grabbing

### Calendar
- not on home page
- dates in hero

### Registration
- N/A