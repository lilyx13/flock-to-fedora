# Competitor/Contemporary Analysis
In the spirit of open source, I am considering these to be more 'contemporaries' than competitors.

## Websites
- [All things Open](https://2021.allthingsopen.org/)
- [Devconf](https://www.devconf.info/cz/)
- [Guadec](https://events.gnome.org/event/9/)
- [Google IO](https://io.google/2021/?lng=en)
- [Open Source Summit](https://events.linuxfoundation.org/open-source-summit-north-america/)

---

## Categories Overview
Differences between mobile and desktop are a key focus in all categories.

### Content Analysis
- Semantic focused critique
- Types of information
- Methods of presentation
- Message clarity

### Layout and General Design
- Background and foreground
- Visual hierarchy
- Layout complexity
- Mobile to desktop experience consistency

### Readability/Scannability
- This category focuses on the content and style of text information
- Includes:
  - long form text
  - headings
  - buttons
  - links

### Topic Review
These are the parts of the site reviewed for nest
- Above the Fold (first content)
- Social Media
- Sponsor Engagement
- Call to Participation 
- Calendar
- Registration

---
