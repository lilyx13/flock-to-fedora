# Intial Site Analysis
[Flock to Fedora](https://flocktofedora.org/)

## Basic info
_who/what/when/where/why/how_
- Note: the old favicon is being used on this site
- Note: there is no language support

### Above the Fold
- Event Description Titles before/at desktop fold
  - 'What is Flock?'
    - Annual Contributor-focused conference
    - Europe/North America/Hopin
  - 'What Will We Do At Flock?'
    - Communicate Fedora Strategy
    - Plan Next Strategy
    - Build Community Connections and Engagement
  - 'Who Comes To Flock?'
    - People who participate in the fedora community, no sessions for regular users
- Event Description Titles before mobile fold
  - 'What is Flock?'
    - Annual Contributor focused conference

#### Content Analysis
- Long form text descriptions
- Speaking to someone who does not know at all what flock is but who knows what Fedora is.
  - provides a link to Fedora mission statement
    - links are in blue and open into new tab
- **Semantic Takeaway**
  - Content should reflect 4 core values and mission statement
  - Currently the descriptions are very long and dense. Bold 1st sentences are helpful, require some insider knowledge.
    - 3 points bold text is focused at very top level when referring to overarching **strategy**

### Schedule and Dates (when)
- a **schedule** link takes user to the fedora wiki which has a calendar of specific events. Opens in same tab
- Calendar Event Dates are not interactive
  - Same information is shown in long form text and visually with a small calendar snippit

#### Content Analysis
- Information is redunant between formats and does not indicate what is happening on any days
  - As schedules are organized up to close to the actual dates it would be hard and probably too much to include lots of content about what's happening on days here. So an external link makes sense.
  - Interaction could improve this - add to calendar? sign up?
  - Keynote speakers could be added to this section?
  - Dates take user to their schedule?

### Call for Participation
- Long form text, initial description points to virtual location of conference
- small text link for submissions
- a 2 sentence long paragraph (long sentences) that describes proposal content type ideas - **storytelling** is key word. **topics** include: team, project, initiative, overall community
- Subheadings
  - Examples
  - Formats
    - List of 5 types of presentations with time lengths (under "General Ideas" heading)
  - Submission Deadline

#### Content Analysis
- While section is focused on participation, the avenues for user interaction are very small and hard to pick out.
- General Ideas heading is confusing
- The examples section is helpful for ideas but as per creating a submission, there is limited guidance on what that will practically look like.

### Registration Information
- 1 line of information
- no button or strong visual indicators
- Reiterates platform

### Social
Outlines different methods of communication for the event
- Social Media subheading
  - event hash tag and fedora twitter link
- Mailing list
  - Noted as primary means of communication
  - second in list
  - most links
- IRC
  - main chanel for events
- Telegram
  - communication during conference

#### Content Analysis
- Text indicates hierarchy and goals of different platforms
  - however you need to read body text to know the differences.
  - use meanings between IRC, Telegram, and Mailing list are confusing

### Venue
- quick note on virtual platform no links or information of what Hopin is

### Sponsors
- Has an icon that is unlike anything else on the page
- contact information for potential future sponsors
- Company logos are large and in 1 vertical column
- Platinum, gold, and bronze levels of sponsors
- Media sponsors
- 'prospectus' link takes a second to realize why you're taken to this pdf. I did not expect for a pdf to open in the browser
- a more streamlined method of signing up might be more effective.
  - regarding the email link, could this be written differently to make it more inviting?
- the prospectus could be better indicated for it's importance

### Footer
- attribution for photograph in hero
- Hero Footer
  - About
  - Download
  - Support
  - Join
- social media icons
  - facebook
  - google
  - twitter
- copyright info
  - copyright text has link to info about relationship between Fedora and Redhat

#### Footer Analysis
- Good general set of links
