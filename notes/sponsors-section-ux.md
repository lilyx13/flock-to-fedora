# Flock to Fedora UX
_Sponsors Section_

## Key Issues
- Sponsors sections can be boring and make a page bloated with content that people don't look at
  - **TODO** make the page engaging to key audiences and not a hassle for nonkey audiences

### Audiences
- Key Audiences
  - Current sponsors
  - Potential sponsors
  - People who the sponsors want to engage
    - Tech people
    - Open source contributors
    - Developers
- Secondary audiences
  - This group is a part of the sponsors audiences but less of a focus.
    - It includes:
      - people who are interested and possibly attending
      - passive end users of open source
    - I'm only loosely separating this group out as it's so closely tied to the main audience
  
### Values
People who go to open source focused events tend to have mild to militant perspectives about ethics and software. People in the Fedora community being quite vocal.

- This is also a community event. Most of the people at this are actively participating in fedora linux in some capacity. Relating Sponsors to this is important for how they are depicted to the attendees and site visitors.
 
#### How to address values
- How they are thanked
  - For supporting, facilitating, and contributing to open source development.
- How they are addressed
  - Location of terminology should reflect roles and relationships between people in their capacities at this event
- How they are presented
  - Convey meaning of sponsor status
    - Beyond just putting a bunch of money in, what does this look like IRL?
    - What is made possible? 
  - A quick blurb from each company on how they are involved in open source or what kind of work they do.
    - **UI IDEA** click on logo will open a pop up with this information that can be clicked away from or that you can access their webpage. Closer to event this should also connect to sessions that they do.
