# Design Planning

This file is my logic outline and brainstorming file to determine how, where, and in what order content will flow. It also addresses visual hierarchy for UI planning.

- Current concerns
  - Page content getting very long and dense
  - Length goal: 6 screenfulls max from top of hero to 1st level of footer

## Page Content Overview

- Nav
  - easy to use --> Build Trust
  - International Support --> Be Welcoming / Share Excitement
- Hero
  - Tagline --> Be welcoming
  - **Action** Relevant CTA --> Share Excitement
    - Register / CFP / Newsletter for reminders
  - Useful info --> Build Trust
  - Social Media Integration --> Create Stuff
- Info/Updates(News)
  - Brief outline of event --> Be welcoming
  - Showcase 3 main types of activities (pres, hackfest, social)
  - Recent Updates --> Share Excitement
    - Event Leadup: Posts about event
    - During Event: Integrate Hopin API to display upcoming events
    - Post Event (Immediate): Wrap up Statement
    - Between Events: Content from last year
  - Social media Links
  - **Action** Learn more
- Topics
  - Types of things covered --> Create Stuff
    - brief descriptions
    - visual indicator + clear title
  - Post Keynote speaker and/or Interactive sessions --> Create Stuff / Build Trust / Be Welcoming
  - **Action** Link to session list / Past Events Youtube
- Schedule
  - Important Dates --> Build Trust
    - Register Open, CFP start / CFP End / Event Dates
  - Event Leadup: Hopin Schedule API --> Share Excitement
  - **Action** Add to Calendar
  - **Action** Newsletter signup
- Sponsors

  - 3 grid levels for sponsors --> Share Excitement / Build Trust
    - Platinum 3 col lg
    - Gold 4 col md
    - Bronze 5 col sm
    - Media Sponsors 2 - 4 (one row)
  - **Action** Go to sponsor icon click (new tab)
  - Sponsorship signup --> Be welcoming
    - **Action** link for sponsorship information

- Footer
  - Social Media links
  - 1st level Hero Footer--> Create Stuff(Features)
    - About | Support | Community
    - **ACTION** Access Fedora web ecosystem
  - Second level Copyright & language --> Build Trust

---

## Principles

_These are some draft guiding principles based on the 4 Foundations of Fedora. Used as a purpose indicator for aspects of this site. Each thing should do at least 1 one of these._

- Be Welcoming -- Friends
- Build Trust -- Freedom
- Share Excitement -- First
- Create Stuff -- Features

---

### User Stories

1. "Oh crap, what's that thing I needed to know again?"

- At first screen, user can:

  - Access Date
  - Access Link to

- Social media would be found on SM platform first and searched their first
- Date information might come to user via email first
  - Not so in case of new attendees
- Registration information
  - Comes from email **OR** this page first
    - Not everyone will engage with this first time around
      - this is key activity when possible
- Schedule
  - time variability (not all info is always known)
  - pin factor (other factors are based on this aspect)
  - High Trust Establishment
  - tied to date
  - Needs to be more specific
  - Importance of types of information change in relevance at different points in the year
    - Likely Multiple Layouts are necessary for this to be functional **THESE ARE ORDERED BY DEGREE OF TIED TO EXTERNAL FACTORS**
      - @ Time when sessions are booked **POST CTP**
        - display embedded calendar
          - _initial thought_ 3 scrollable columns on desktop
            - Mobile:
              - 1st idea: tab or slider to select day and calendar shows up
                - I'm liking the swipe date because at start all 3 can be visible at low opacity
              - 2nd idea: horizontal swipe (thought b/c chronological this might be weird)
      - @ Time when event is over but content hasn't been fully organized yet **POST EVENT**
        - No schedule needs to be shown
        - Important Time Relevant Post Event Things (Immediate Follow Up Stuff):
          - Wrap up statement
          - Event content posted (videos, blogs...) -> Need to give notice that this is a thing (high value placeholder)
          - Thank you
          - Attendees may need to follow up on certain things that will be fresh in mind -> this content block should be helpful there
      - @ Time long between events
        - Last years content available
        - Dates for next year
          - Ability to add a notification in personal calendar
        - Notice on CFP open date

2. New Visitor Scenarios
   (Target audience is people who use fedora or could benefit from using fedora as a platform to create stuff **OR** people who are thinking about becoming fedora contributors)

- Not really sure what this is but wants to know more. like 5 second attention span
  - Hero explains:
    - Contributor conference, dates, where things currently are timeline wise
  - Hero demonstrates:
    - Vibrant active global tech community
  - **IF user is attracted to this**
    - next section gives more detail
      - overarching goal of the event - say
      - things that you can do at this event - show
      - examples -show
  - Vistor can sign up or learn more at this point
    - Vistor can also keep scrolling and learn more specific details
- Has used fedora/uses fedora and is working on something unrelated to fedora with it. Potential Presenter
  - Hero section
    - Gives ability to register and submit proposal
    - Should make them feel like this is a place they want to present at
  - Info section
    - Information (update or in statement) that will tell story that makes them assume that this is a place for them. Builds excitement to present and to see other presentations
  - Topics
    - Main section for this user story. Language that supports their field's inclusion should be here
    - Can learn more if needed
    - should be able to get back to registration and CFP very quickly
- Fedora user who found out about this and is feeling inspired/curious
  - Could be potentially alienated by amount of jargon terms needed to appeal to main attendee bases
    - If it's not for them, perhaps a shoutout to release parties as being more suited to end users?
    - If still interested, topics section should note learning opportunities, interactive events, and networking opportunities
      - Able to register quickly from here

## How to display event content?

- A general topics list that relates to main content and speaks to particular attendee groups
  - based on different areas of development
    - Review last few years and find common categories
    - Include a 1-2 phrase/sentence outline of the category
    - There should be a link to more details
      - upcomming session?
      - past session?
  - Use a type of list format, make use of horizontal space
    - on mobile, this should condense down - dropdown on click for descriptions?
- Session types as cards
  - Lower down the page
  - Outline 3 main types of events
    - Presentations
    - Hackfests
    - Socials

### Visual Elements

- Well used icons may help with this part of the site
- Fedora Badges may also be able to be used. However these might be better in a more community focused area

---

## Section Design Content Brainstorm Detailed

### Hero Section

This will be arguably the most important part of this website.

#### Essentials

- Image that depicts:
  - Community
  - Fedora Brand
  - Inspiration
- Headline:
  - 1 Line that describes conference
- Date Information
  - Upcoming conference date
- CTA:
  Time sensitive, depending on upcoming deadlines, will determine which buttons are available. 1-2 Buttons.
  - Possible Actions:
    - Register
    - Call for Proposals
    - Explore the Schedule
    - Event Recap
- Large Well Defined Logo

### Navigation

**Concept 1 is what I'm going to start with**

- Desktop

  1. One Nav Locations

  - Links away from page in icons top bar
  - Anchor links across top bar
  - Use spacing or a | between them
  - One tobar item should be styled like a button
  - Penpot is a good example (desktop and mobile)

  2. Two Nav Locations

  - Links away from page in top bar
  - Side bar nav links down the page
    - sticky over content
    - not obtrusive

- Mobile

1. One Nav Location

- hamburger menu
- Divider in menu for anchor links and external links

2. Two Nav Location

- Hamburger menu
  - External Links
- Side or Bottom links vertical
  - Local Anchor Links

### Footer

- Use start.fpo footer design

###
