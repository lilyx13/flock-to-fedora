# README
This repo is for organizing UI/UX design content.

- Note: Flock email notifications need to have a link to the flock website
  - This is to give access to:
    - recent events
  - Because an email link should send the user straight to the registration page first
    - Therefore this page will be more supporting information
      - Links just as text will look unappealing. Icon or helpful context information could make this better
        - otherwise it's really just a formality in how much attention it can draw
          - deal with this issue by having 2 clear and distinct links on the notification --> To registration and to homepage for more info
## Organization

- `assets/`: design assets such as icons, images, fonts, and styles
- `design/`: design files and static png files showing mockups
- `interviews/`: Content from stakeholder meetings. Note that only my notes will be visible, audio/video content will be hidden.
- `moodboard/`: images of my brainstorming and planning webs. These are being created in penpot.
- `review/`: these are files used for the initial/final Flock page analyses as well as the competitor analysis.
